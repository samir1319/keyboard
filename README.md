# Remote Keyboard Application

This is a simple browser-based Remote Keyboard application that allows two users to share a virtual keyboard on a grid of 10 keys. Each user can acquire control of the keyboard and light up the keys. The application uses PHP, MySQL, jQuery, and HTML.

## Features

- Two users can access the keyboard simultaneously.
- The keyboard consists of a 2 x 5 grid with keys of size 50px x 50px.
- Initially, all keys are turned off (white color).
- When a user clicks on any key, it turns to a specific color (red for user 1, yellow for user 2).
- Users can click on already lit keys to turn them off (white color).
- Users need to acquire control of the keyboard before they can light up keys.
- Only one user can have control at a given time.
- The control has a timeout of 120 seconds, after which it will be automatically released.
- When a user lights or unlights a key, the changes are reflected on the other user's screen without page refreshing (using Ajax polling).

## Prerequisites

- Local MySQL server set up.
- PHP and Apache installed (XAMPP, WAMP, or any other stack).

## Installation

1. Clone the repository using Git:

git clone https://gitlab.com/samir1319/keyboard.git



2. Set up the MySQL database:

   - Create a new database named `remote_keyboard`.
   - Import the `remote_keyboard.sql` file located in the `database` folder into the `remote_keyboard` database to create the required tables.

3. Copy the cloned `keyboard` directory into your web server's root directory (e.g., `htdocs` in XAMPP).

4. Configure the database connection in `backend.php`:

   - Open `backend.php` located in the `keyboard` directory.
   - Edit the following lines to set your MySQL database credentials:

   ```php
   $db_host = 'localhost';
   $db_username = 'your_mysql_username';
   $db_password = 'your_mysql_password';
   $db_name = 'remote_keyboard';

```

Start your local web server (Apache) and MySQL server.

Access the application from your web browser:

```http://localhost/keyboard```


Usage
Open the application in two different browsers or machines to simulate two users.
User 1 and User 2 can take turns acquiring control by clicking the "Acquire Control" button.
The user with control can click on the keys to light them up (red for User 1, yellow for User 2).
If a key is already lit, clicking on it will turn it off (white color).
The control will be automatically released after 120 seconds of inactivity.