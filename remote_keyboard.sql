-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2023 at 07:56 PM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `remote_keyboard`
--

-- --------------------------------------------------------

--
-- Table structure for table `keyboard_state`
--

CREATE TABLE `keyboard_state` (
  `key_id` int(11) NOT NULL,
  `key_color` varchar(10) DEFAULT NULL,
  `control_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `keyboard_state`
--

INSERT INTO `keyboard_state` (`key_id`, `key_color`, `control_timestamp`) VALUES
(1, 'yellow', '2023-08-04 17:49:41'),
(2, 'yellow', '2023-08-04 17:49:26'),
(3, 'red', '2023-08-04 17:50:12'),
(4, 'yellow', '2023-08-04 17:49:32'),
(5, 'yellow', '2023-08-04 17:49:29'),
(6, 'yellow', '2023-08-04 17:49:50'),
(7, 'yellow', '2023-08-04 17:47:13'),
(8, 'red', '2023-08-04 17:55:42'),
(9, 'red', '2023-08-04 17:55:51'),
(10, 'red', '2023-08-04 17:55:52');

-- --------------------------------------------------------

--
-- Table structure for table `user_control`
--

CREATE TABLE `user_control` (
  `user_id` int(11) NOT NULL,
  `control_key_id` int(11) DEFAULT 0,
  `control_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_control`
--

INSERT INTO `user_control` (`user_id`, `control_key_id`, `control_timestamp`) VALUES
(1, 1, '2023-08-04 17:55:38'),
(2, 0, '2023-08-04 17:55:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `keyboard_state`
--
ALTER TABLE `keyboard_state`
  ADD PRIMARY KEY (`key_id`);

--
-- Indexes for table `user_control`
--
ALTER TABLE `user_control`
  ADD PRIMARY KEY (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
