<?php
// Connect to the MySQL database
$hostname = 'localhost';
$username = 'root';
$password = '';
$database = 'remote_keyboard';

$db_connection = mysqli_connect($hostname, $username, $password, $database);
if (!$db_connection) {
    die('Database connection failed.');
}

function getKeyboardState()
{
    global $db_connection;
    $query = "SELECT * FROM keyboard_state";
    $result = mysqli_query($db_connection, $query);
    $keyboard_state = array_fill(1, 10, 'white'); 

    while ($row = mysqli_fetch_assoc($result)) {
        $key_id = $row['key_id'];
        $key_color = $row['key_color'];
        $keyboard_state[$key_id] = $key_color;
    }

    return $keyboard_state;
}


function updateKeyColor($user_id, $key_id, $key_color)
{
    global $db_connection;
    $query = "UPDATE keyboard_state SET key_color = '$key_color', control_timestamp = NOW() WHERE key_id = $key_id";
    mysqli_query($db_connection, $query);
}


// Function to acquire control
function acquireControl($user_id)
{
    global $db_connection;
    $query_check_control = "SELECT control_key_id FROM user_control WHERE user_id = $user_id";
    $result_check_control = mysqli_query($db_connection, $query_check_control);
    $row_check_control = mysqli_fetch_assoc($result_check_control);

    if ($row_check_control['control_key_id'] > 0) {
        echo 'failure';
        return;
    }

    mysqli_query($db_connection, "UPDATE user_control SET control_key_id = 0");
    // Acquire control
    $query_acquire_control = "UPDATE user_control SET control_key_id = 1, control_timestamp = NOW() WHERE user_id = $user_id";
    if (mysqli_query($db_connection, $query_acquire_control)) {
        echo 'success';
    } else {
        echo 'failure';
    }
}


function checkaquireduser($user_id)
{
    global $db_connection;
    $query_check_control = "SELECT control_key_id FROM user_control WHERE user_id = $user_id";
    $result_check_control = mysqli_query($db_connection, $query_check_control);
    $row_check_control = mysqli_fetch_assoc($result_check_control);

    if ($row_check_control['control_key_id'] > 0) {
        echo 'success';
        return;
    }else{
        echo 'failure';
        return;
    }
}

// Function to release control after 120 seconds of inactivity
function releaseControl($user_id)
{
    global $db_connection;
    $query_release_control = "UPDATE user_control SET control_key_id = 0, control_timestamp = NULL WHERE user_id = $user_id";
    mysqli_query($db_connection, $query_release_control);
}

// Check for AJAX request and handle accordingly
if (isset($_POST['action'])) {
    if ($_POST['action'] === 'get_keyboard_state') {
        $keyboard_state = getKeyboardState();
        echo json_encode($keyboard_state);
    } elseif ($_POST['action'] === 'update_key_color') {
        $key_id = $_POST['key_id'];
        $key_color = $_POST['key_color'];
        $user_id = $_POST['user_id'];
        updateKeyColor($user_id,$key_id, $key_color);
    } elseif ($_POST['action'] === 'acquire_control') {
        $user_id = $_POST['user_id'];
        acquireControl($user_id);
    }elseif ($_POST['action'] === 'checkaquireduser') {
        $user_id = $_POST['user_id'];
        checkaquireduser($user_id);
    }elseif ($_POST['action'] === 'release_control') {
        $user_id = $_POST['user_id'];
        releaseControl($user_id);
    }
}
// Close the database connection
mysqli_close($db_connection);
?>
