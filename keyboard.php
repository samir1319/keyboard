<!DOCTYPE html>
<html>
<head>
    <title>Simple Remote Keyboard</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <style>
        .keyboard {
            display: grid;
            grid-template-columns: repeat(5, 50px);
            gap: 5px;
            margin-top: 20px;
        }
        .key {
            width: 50px;
            height: 50px;
            background-color: white;
            border: 1px solid black;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <h1>Simple Remote Keyboard</h1>
    <div class="keyboard">
        <?php
            for ($i = 1; $i <= 10; $i++) {
                echo "<div class='key' data-key='$i'>$i</div>";
            }
        ?>
    </div>
    <input type="hidden" id="currentUserId" value="<?=$_GET['user']?>">
    <button id="controlButton">Take Control</button>
</body>
<script type="text/javascript">
	$(document).ready(function() {

    let currentUserId = $('#currentUserId').val(); // Get the current user's ID
    let hasControl = false; // Flag to track if t
    let inactivityTimer = null;

    // Function to update the keyboard grid based on the keyboard state
    function updateKeyboardState(keyboard_state) {
        for (let key_id = 1; key_id <= 10; key_id++) {
            const key_color = keyboard_state[key_id];
            $(`.key[data-key='${key_id}']`).css('background-color', key_color);
        }
    }


    function handleKeyClick(key_id) {
    	//alert(checkaquireduser());
    	if(!checkaquireduser()){
    		alert("You don't have control of the keyboard. Please acquire control first.");
            return false;
    	}

        const key_color = $(`.key[data-key='${key_id}']`).css('background-color');
        const new_key_color = key_color === 'rgb(255, 255, 255)' ? (currentUserId === '1' ? 'red' : 'yellow') : 'white';
        updateKeyColorOnServer(currentUserId, key_id, new_key_color);
    }

    function resetInactivityTimer() {
        clearTimeout(inactivityTimer);
        inactivityTimer = setTimeout(releaseControl, 120000); // 120 seconds = 120,000 milliseconds
    }


    // Function to handle control button click event
    function acquireControl() {
        $.ajax({
            url: 'backend.php',
            type: 'POST',
            data: {
                action: 'acquire_control',
                user_id: currentUserId,
            },
            success: function(response) {
                if (response === 'success') {
                    alert('You have acquired control of the keyboard.');
                    hasControl = true; // Set the flag to indicate control
                } else {
                    alert('You already have control of the keyboard.');
                }
            },
        });
    }


    function checkaquireduser() {
    	var retval = false;
        $.ajax({
            url: 'backend.php',
            type: 'POST',
            data: {
                action: 'checkaquireduser',
                user_id: currentUserId,
            },
            async:false,
            success: function(response) {
                if (response === 'success') {
                    retval = true; // Set the flag to indicate control
                }
            },
        });
        return retval;
    }


    function releaseControl() {
        $.ajax({
            url: 'backend.php',
            type: 'POST',
            data: {
                action: 'release_control',
                user_id: currentUserId,
            },
            success: function() {
                alert('You have released control of the keyboard.');
                hasControl = false; // Set the flag to indicate no control
            },
        });
    }

    // Function to update the key color on the server
    function updateKeyColorOnServer(user_id,key_id, key_color) {
        $.ajax({
            url: 'backend.php',
            type: 'POST',
            data: {
                action: 'update_key_color',
                key_id: key_id,
                key_color: key_color,
                user_id: user_id,
            },
        });
    }

    // Function to get the current keyboard state from the server
    function getKeyboardStateFromServer() {
        $.ajax({
            url: 'backend.php',
            type: 'POST',
            dataType: 'json',
            data: {
                action: 'get_keyboard_state',
                user_id: currentUserId,
            },
            success: function(response) {
                updateKeyboardState(response);
            },
        });
    }

    // Attach click event to keys
    $(document).on('click', '.key', function() {
        const key_id = $(this).data('key');
        handleKeyClick(key_id);
        resetInactivityTimer(); // Release control after clicking any key
    });

    $(document).on('mousemove keydown', function() {
        resetInactivityTimer();
    });

    // Attach click event to control button
    $('#controlButton').on('click', function() {
        acquireControl();
        resetInactivityTimer();
    });

    // Get the initial keyboard state and start polling for updates
    getKeyboardStateFromServer();
    setInterval(function() {
        //resetInactivityTimer();  // Check for control release every 1 seconds
        getKeyboardStateFromServer();
    }, 1000);
});


</script>
</html>
